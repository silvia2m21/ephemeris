### **Ephemeris**

Run the following commands in prompt with sudo:

`$ pip3 install pytest` \
`$ pip3 install numpy` \
`$ pip3 install astropy` \
`$ pip3 install jdcal` \
`$ pip3 install git+https://github.com/astropy/astroquery.git` \
`$ pip3 install git+https://github.com/NASA-Planetary-Science/sbpy.git `


Copy file `configuration_example.py` into `configuration.py` and change it with your configuration.
 
Run the main file:

`$ python3 main.py`