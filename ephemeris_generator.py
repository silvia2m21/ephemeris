import re
import os
import csv
import jdcal
from astroquery.jplhorizons import Horizons


def create_aas(comets,elem):

    if os.path.exists('aas'):
        os.remove('aas')

    with open(comets, 'r') as f:
        with open('aas', 'a') as f1:

            for ln in f:
                sb = re.sub(' +', ' ', ln)
                sp = sb.split(' ')

                with open(elem) as f2:
                    for ln2 in f2:
                        sb2 = re.sub(' +', ' ', ln2)
                        sp2 = sb2.split(' ')

                        if (sp[0] == sp2[1]) & (sp[1] == '4\n'):
                            if (sp2[9] == '2016') & (sp2[10] == 'J1-A'):
                                f1.write('90004268'+'\n')
                            elif (sp2[9] == '2013') & (sp2[10] == 'R3'):
                                f1.write('90004085' + '\n')
                            elif ln2[92] != ' ':
                                f1.write(sp2[9]+'P'+'\n')
                            else:
                                f1.write(sp2[9]+' '+sp2[10]+'\n')


def eph_filter(YYYY,MM,DD,acos,Vlim):

    date = int(sum(jdcal.gcal2jd(YYYY,MM,DD)))+0.5

    if os.path.exists('selected'):
        os.remove('selected')

    with open(acos,'r') as f:
        with open('selected','a') as ff:
            for ln in f:
                sb = re.sub(' +', ' ', ln)
                sp = sb.split(' ')

                if sp[0] == '':
                    if len(sp[12])<7:
                        name = sp[11]
                    else:
                        name = sp[11]
                else:
                    if len(sp[11])<7:
                        name = sp[10]
                    else:
                        name = sp[10][0:4] + ' ' + sp[10][4:]

                if (name != '2017 CD39') and (name != '2013 QQ95'):

                    obj = Horizons(id=name, location='500', epochs=date)
                    eph = obj.ephemerides(quantities='1,9,23,41')

                    elong = eph['elong'][0]
                    try:
                        V = eph['V'][0]
                    except:
                        V = eph['Tmag'][0]

                    try:
                        true_anom = eph['true_anom'][0]

                        if (elong>60) & (V<int(Vlim)) & (true_anom < 60 or true_anom > 300):
                            ff.write(name + '\n')

                    except:
                        print("")

    with open('aas') as f:
        with open('selected','a') as ff:
            for ln in f:

                name = ln
                obj = Horizons(id=name, location='500', epochs=date)
                eph = obj.ephemerides(quantities='1,9,23')

                elong = eph['elong'][0]
                try:
                    V = eph['V'][0]
                except:
                    V = eph['Tmag'][0]

                try:
                    true_anom = eph['true_anom'][0]

                    if (elong>60) & (V<int(Vlim)) & (true_anom < 60 or true_anom > 300):
                        ff.write(name)
                except:
                    print("")


def ephemeris(YYYY,MM,DD,loc):

    if os.path.exists('ephemeris.csv'):
        os.remove('ephemeris.csv')

    with open('selected','r') as f:
        with open('ephemeris.csv','w') as ff:
            for ln in f:

                DD2 = str(int(DD) + 1)

                obj = Horizons(id=ln, location=loc,
                               epochs={'start': YYYY + '-' + MM + '-' + DD + ' 21:00:00', 'stop': YYYY + '-' +
                                                MM + '-' + DD2 + ' 08:00:00', 'step':'30m'})
                eph = obj.ephemerides(skip_daylight=True, quantities='1,3,4,9,23,41')

                #eph = MPC.get_ephemeris(ln, location=loc, start= YYYY + '-' + MM + '-' + DD + ' 00:00:00', step='1h', number=9)

                #print(eph)

                writer = csv.writer(ff)
                [writer.writerow(r) for r in eph]


def ephemeris_final():

    with open('ephemeris.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        with open('ephemeris_final.csv','w') as f:
            for row in csv_reader:
                if len(row) < 18:
                    if float(row[12]) > 22 and float(row[12]) < 72:
                        writer = csv.writer(f)
                        writer.writerow(row)
                else:
                    if float(row[15]) > 22 and float(row[12]) < 72:
                        writer = csv.writer(f)
                        writer.writerow(row)

    os.remove('aas')
    os.remove('selected')
    os.remove('ephemeris.csv')

    print("Ephemeris generated for the date, file ephemeris_final.csv")