
def config():

    # Change your configurations:

    # YYYY = year
    # MM = month
    # DD = day
    # acos_date = date of last run of acos program
    # Vlim = limiting visual magnitude
    # location = observatory code
    # ast_elem = path of file with asteroids elements
    # com_elem = path of file with comet elements
    # criterion = path for file comet.txt
    # results = where you want to put your results

    #####################################################

    YYYY = '2019'
    MM = '06'
    DD = '03'
    acos_date = '05-2019'
    Vlim = '21'
    location = 'Y28'
    ast_elem = '/home/username/results/aster/elements/'
    com_elem = '/home/username/results/comet/elements/'
    criterion = '/home/username/results/aster/criterion/'
    results = '/home/username/ephemeris/'

    #####################################################

    acos = ast_elem + acos_date + '/elem_acos_final'
    elem =  com_elem + acos_date + '/elem_jpl'
    comets = criterion + acos_date + '/comet.txt'


    return YYYY, MM, DD, acos_date, Vlim, location, acos, elem, comets, results