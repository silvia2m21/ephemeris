import os
import shutil
from ephemeris_generator import create_aas,eph_filter,ephemeris,ephemeris_final
from configuration import config

YYYY, MM, DD, acos_date, Vlim, loc, acos, elem, comets, results = config()
os.chdir(results)
current_date = MM + '-' + YYYY

if os.path.exists(current_date):
    shutil.rmtree(current_date)
os.mkdir(current_date)
os.chdir(current_date)

create_aas(comets,elem)
eph_filter(YYYY,MM,DD,acos,Vlim)
ephemeris(YYYY,MM,DD,loc)
ephemeris_final()
